﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2A203_21_CTP1
{
    public partial class Form1 : Form
    {

        // Hna fin ghadi n3ayat 3la Maystro dyal la base
        private Database database = new Database();

        public Form1()
        {
            InitializeComponent();
        }


        // Had Form1_Load hya li mkalfa bach dir chi traitement mnin yallah t7al Fenetre
        // Awal 7aja tat executa mnin tat7al fenetre hya had Form1_Load
        // Ya3ni ila 3ndk chi khdma dirha f demmarage dyal app dirha hna
        // B7al dir connexion la base matalan
        private void Form1_Load(object sender, EventArgs e)
        {
            //1- Ghadi nlancer connexion
            database.Connecter();

            //2- Ndiro Refresh l Grid View bach ibano fiha les données dyal La BASE
            RefreshDataGrid();
        }

        private void Form1_Leave(object sender, EventArgs e)
        {
            database.Deconnecter();
        }


        // Raddo lbal les methdes taybdaw b Maj
        // Had method ghatkhli DataGrid dir refrsh 
        // Ya3ni ghada dir mise à jour ila darti Ajouter/Supprimer/Modifier
        public void RefreshDataGrid()
        {

            // ila kant GridView Khawya bla matkhwiha
            if (database.dataTable.Rows != null)
            {
                database.dataTable.Clear();
            }

            database.cmd.CommandText = "SELECT * FROM Etudiant";
            
            // Dart had lblan bach ngol l'objet Commande bli ghada diri execution dyal had commande b cnx li deja 3ndi
            // cnx li deja 3ndi rah fiha cnx (mconnectya)
            database.cmd.Connection = database.cnx;

            // 7na mazal m3a CMD (Commande)
            // Ghanjibo resultat dyal dik SELECT
            database.dataReader = database.cmd.ExecuteReader();

            // DataTable hya la table
            // DataReader hya data li jat mn 3nd SELECT
            // Data li jat mn 3nd select an7atoha f DataTable 
            // Bach twali tableau 3an nkhadmoha f GRID
            // 7it Data Grid matata3rfch DataAdapter ola Data Reader
            // bach n3amro DATA TABLE tandiro had star
            database.dataTable.Load(database.dataReader);

            // matnssawch close dataReader
            database.dataReader.Close();

            // Héeeeeeeeeeeeeeeeeeeeeeeeeeeee
            // 3mrat
            dataGridView1.DataSource = database.dataTable;

        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            // Matnssawch bli cmd rah machi hya li tan7atto fiha query dyalna
            // rah cmd.CommandText 

            
            
            // ----------------   1   ----------------------
            // -------------------------------------------
            // Kayna wa7d methode ila dartiha dir gha had star
            // tatzid $ flawal
            
            // database.cmd.CommandText = $"INSERT INTO Etudiant values({numID.Value}, '{txtNom.Text}', '{txtPrenom.Text}', {numAge.Value})";
           
            //----------------------------------------------
            //----------------------------------------------





            // ----------------   2    ---------------------
            // -------------------------------------------
            // Kayna had methode ila khdmtiha dir had stora
            // mafihach $ flawal
            database.cmd.CommandText = "INSERT INTO Etudiant values(@id, @nom, @prenom, @age)";

            // awalan 7ayad ga3 les parametres l9daaaaaaaaaaam bach matl9ach rassk msift la base chi7aja khra
            database.cmd.Parameters.Clear();

            // wila darti had tari9a aykhssk t3mr dok les parametres 
            database.cmd.Parameters.AddWithValue("@id", numID.Value);
            database.cmd.Parameters.AddWithValue("@nom", txtNom.Text);
            database.cmd.Parameters.AddWithValue("@prenom", txtPrenom.Text);
            database.cmd.Parameters.AddWithValue("@age", numAge.Value);
            //----------------------------------------------
            //----------------------------------------------


            database.cmd.ExecuteNonQuery();

            RefreshDataGrid();

        }

        private void Fermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
